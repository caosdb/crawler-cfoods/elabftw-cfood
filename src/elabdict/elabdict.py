#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2024 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#               2024 IndiScale GmbH <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

"""
Custom converter that reads metadata from JSON files and modifies it such that the kind of JSON can
be distinguished by the cfood
"""

import re

from caoscrawler.converters import DictConverter
from caoscrawler.stores import GeneralStore
from caoscrawler.structure_elements import DictElement, File, StructureElement


class eLabDictConverter(DictConverter):
    def create_children(self, generalStore: GeneralStore, element: StructureElement):
        if not isinstance(element, DictElement):
            raise ValueError("create_children was called with wrong type of StructureElement")
        if "type" in element.value:  # single experiment or item
            if element.value["type"] == "experiments":
                element.value = {"experiment": element.value}
            elif element.value["type"] == "items":
                element.value = {"item": element.value}
        return super().create_children(generalStore, element)
