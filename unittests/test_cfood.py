#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2024 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2024 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

"""
test for elabftw cfood
"""
import json
import logging
import os

import linkahead as db
from caoscrawler.converters import DictConverter, JSONFileConverter
from caoscrawler.crawl import Crawler
from caoscrawler.identifiable_adapters import CaosDBIdentifiableAdapter
from caoscrawler.scanner import (create_converter_registry, load_definition,
                                 scan_structure_elements)
from caoscrawler.structure_elements import Directory, File, JSONFile

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def rfp(*pathcomponents):
    """
    Return full path.
    Shorthand convenience function.
    """
    return os.path.join(os.path.dirname(__file__), *pathcomponents)


def test_cfood():
    crawler_definition_path = rfp("../cfood.yml")

    # ident = CaosDBIdentifiableAdapter()
    # ident.load_from_yaml_definition(rfp("../identifiables.yml"))

    crawler_definition = load_definition(crawler_definition_path)
    scanned = scan_structure_elements(
        items=JSONFile('test', rfp("test_data/experiments_example.json")),
        crawler_definition=crawler_definition,
        converter_registry=create_converter_registry(crawler_definition)
    )

    print(scanned)

    inst_candiates = [el for el in scanned if el.parents[0].name == "eLabFTWInstance"]
    assert len(inst_candiates) == 1
    assert inst_candiates[0].get_property('url').value == "https://demo.elabftw.net"
    exp_candiates = [el for el in scanned if el.parents[0].name == "eLabFTWExperiment"]
    assert len(exp_candiates) == 2
    exp = None
    for ec in exp_candiates:
        if ec.get_property("category") is not None:
            exp = ec
    assert exp.name == "Rerum eum molestiae nemo corporis est molestias quis eos."
    assert exp.description == "Would test."
    assert exp.get_property("category").value == "Project CASIMIR"
    assert exp.get_property("date").value == "2023-04-06"
    assert exp.get_property("externalID").value == "202"
    assert exp.get_property(
        "eLabFTWUniqueID").value == "20240102-05ecdc3b4eb777c5a66f0450a726a7a7d0e0592b"
    assert exp.get_property("eLabFTWExperiment") is not None
    ref = exp.get_property("eLabFTWExperiment").value[0]
    assert ref.get_property("externalID").value == '191'
    assert exp.get_property("eLabFTWUser") is not None
    pers = exp.get_property("eLabFTWUser").value
    assert pers.get_property("firstname").value == "Augusta"
    assert pers.get_property("lastname").value == "White"
    assert pers.get_property("externalID").value == "4"
    assert pers.get_property("eLabFTWInstance").value.get_property(
        "url") == inst_candiates[0].get_property("url")
    assert exp.get_property("eLabFTWItem") is not None
    item = exp.get_property("eLabFTWItem").value[0]
    assert item.get_property("externalID").value == '133'
    assert "customoptions" in exp.get_property("metadata").value
    assert exp.get_property("next_step").value == 'dry'
    assert exp.get_property("rating").value == '0'
    assert exp.get_property("state").value == '1'
    assert exp.get_property("status").value == 'Running'
    assert 'biochemistry' in exp.get_property("tags").value
    assert "demo.elab" in exp.get_property("url").value
    assert exp.get_property("eLabFTWUpload") is not None
    up = exp.get_property("eLabFTWUpload").value[0]
    assert up.get_property(
        "hash").value == "9926ccb1fb0fde79db736b21c375c8d6c8135e9232851c62bcf1a7beb2a581d7"
    assert up.get_property("filename").value == "log23.txt"
    assert up.get_property("hash_algorithm").value == "sha256"
    assert up.get_property("externalID").value == "70"
    com_candiates = [el for el in scanned if el.parents[0].name == "CommentAnnotation"]
    assert len(com_candiates) == 1
    assert com_candiates[0].get_property('comment').value == "comm<br />\n"

    logger.debug("new")
    scanned = scan_structure_elements(
        items=JSONFile('test', rfp("test_data/experiments_api_list.json")),
        crawler_definition=crawler_definition,
        converter_registry=create_converter_registry(crawler_definition)
    )

    exp_candiates = [el for el in scanned if el.parents[0].name == "eLabFTWExperiment"]
    assert len(exp_candiates) == 16


if __name__ == "__main__":
    test_dataset()
