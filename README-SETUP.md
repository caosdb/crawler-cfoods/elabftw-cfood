WORK IN PROGRESS

This is so far only a proof of concept implementation.

- The data mapping is defined in `cfood.yml` (see. https://gitlab.com/caosdb/caosdb-crawler/)
- The identifiables are defined using `identifiables.yml`
- There is an example data file `experiments_example.json` which you can use if you do not have access to a ELabFTW instance. You need to adjust crawl.py accordingly.
- ElabFTW can define Item Types which currently need to be added to the data model and to the list of identifiables. The cfood should be independent of new Item Types.

In order to run this example, you need to:
1. insert the data model into your CaosDB instance: `python insert_model.py`
2. adjust the cfood.yml (look for all capital place holders)
3. run `crawl.py`
