#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2022 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
import logging
from tqdm import tqdm
import elabapy
import json
from caoscrawler.identifiable_adapters import CaosDBIdentifiableAdapter
from caoscrawler.converters import JSONFileConverter
from caoscrawler import Crawler
from caoscrawler.structure_elements import File, JSONFile, Directory
from requests.exceptions import HTTPError
import caosdb as db

URL = "https://localhost/api/v1/"
# URL = "https://demo.elabftw.net/api/v1/"
TOKEN = "token"
manager = elabapy.Manager(endpoint=URL, token=TOKEN, verify=False)

slim_experiments = manager.get_all_experiments({'limit': 100000})
experiment_ids = [el["id"] for el in slim_experiments]
experiments = {}
for eid in tqdm(experiment_ids):
    experiments[eid] = manager.get_experiment(eid)

with open('experiments.json', 'w') as f:
    try:
        f.write(json.dumps(experiments, indent=4))
        f.write("\n")
    except:
        pass


crawler_definition_path = "./cfood.yml"
# json_file_path = rfp("test_directories", "single_file_test_data", "testjson.json")
log = logging.getLogger()
log.setLevel(logging.DEBUG)

ident = CaosDBIdentifiableAdapter()
ident.load_from_yaml_definition("identifiables.yml")
crawler = Crawler(debug=True, identifiableAdapter=ident)
crawler_definition = crawler.load_definition(crawler_definition_path)
# Load and register converter packages:
converter_registry = crawler.load_converters(crawler_definition)

records = crawler.start_crawling(
    JSONFile('data', "./experiments_example.json"), crawler_definition, converter_registry,
)

# crawler.save_debug_data("provenence.yml")
print(records)
crawler.synchronize(unique_names=False)
