/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021,2023,2024 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 * Copyright (C) 2023,2024 Henrik tom Wörden <h.tomwoerden@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 */

"use strict";

/*
 * Define the parent object that will contain the input form and the
 * button to trigger it that will be appended onto the LinkAhead main
 * panel in the webinterface
 */
var crawlermenu = new function (form_elements, form_panel, isAuthenticated) {
    this.init = function () {

        // only show the button and the form to logged in users
        if (isAuthenticated()) {
            this.raw_data();
        }
    }

    const accepted_file_formats = [
        ".json",
        ".JSON",
        "application/json",
        "text/json",
    ]

    const export_csv_form_config = {
        script: "crawl.py",
        fields: [{
            type: "file",
            name: "jsonfile",
            label: "eLabFTW Export JSON",
            required: true,
            cached: false,
            accept: accepted_file_formats.join(","),
            help: "Select JSON export that you want to upload."
        }],
    };

    this.raw_data = function () {
        const export_csv_title="Import eLabFTW JSON";
        const csv_callback = 
        navbar.add_tool(export_csv_title, "Import", {
            callback: form_panel.create_show_form_callback(
                "crawl_elab_json_id",
                export_csv_title,
                export_csv_form_config
            )
        });
    }
}(form_elements, form_panel, isAuthenticated);

/*
 * Finally, add the from button to the main panel when the page is shown
 */
$(document).ready(function () {
    crawlermenu.init();
});
