#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2023,2024 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2023,2024 Henrik tom Wörden <h.tomwoerden@indiscale.com>
# Copyright (C) 2023,2024 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

import json
import logging
import os
import sys

import linkahead as db
import pandas as pd
from caosadvancedtools.serverside import helper
from caoscrawler import Crawler, SecurityMode
from caoscrawler.crawl import (_check_record_types,
                               _notify_about_inserts_and_updates)
from caoscrawler.identifiable_adapters import (CaosDBIdentifiableAdapter,
                                               IdentifiableAdapter,
                                               LocalStorageIdentifiableAdapter)
from caoscrawler.logging import configure_server_side_logging
from caoscrawler.scanner import (create_converter_registry,
                                 initialize_converters, load_definition,
                                 scan_directory, scan_structure_elements)
from caoscrawler.structure_elements import JSONFile

# suppress warning of diff function
apilogger = logging.getLogger("caosdb.apiutils")
apilogger.setLevel(logging.ERROR)


def main():
    parser = helper.get_argument_parser()
    args = parser.parse_args()
    db.configure_connection(auth_token=args.auth_token)
    # setup logging and reporting if serverside execution
    userlog_public, htmluserlog_public, debuglog_public = configure_server_side_logging()

    if not hasattr(args, "filename") or not args.filename:
        raise RuntimeError("No file provided!")

    cfood_file_name = os.path.expanduser("~/cfood.yml")
    upload_dir = os.path.dirname((args.filename))
    # Read the input from the form (form.json)
    with open(args.filename) as form_json:
        form_data = json.load(form_json)
    # Read content of th uplaoded file
    jsonfilepath = os.path.join(upload_dir, form_data["jsonfile"])
    crawler = Crawler(securityMode=SecurityMode.UPDATE)

    crawler_definition = load_definition(cfood_file_name)
    # Load and register converter packages:
    converter_registry = create_converter_registry(crawler_definition)

    crawled_data = scan_structure_elements([JSONFile('elabjson', jsonfilepath)],
                                           crawler_definition,
                                           converter_registry)
    _check_record_types(crawled_data)

    identifiables_definition_file = os.path.expanduser("~/identifiables.yml")

    ident = CaosDBIdentifiableAdapter()
    ident.load_from_yaml_definition(identifiables_definition_file)
    crawler.identifiableAdapter = ident

    inserts, updates = crawler.synchronize(commit_changes=True,
                                           unique_names=False,
                                           crawled_data=crawled_data,
                                           path_for_authorized_run='/')


if __name__ == "__main__":
    main()
