
# README

## Welcome
THIS IS A PROOF OF CONCEPT AND STILL WORK IN PROGRESS

This is a module for the [LinkAhead-Crawler](https://gitlab.com/linkahead/linkahead-crawler/)
that can read data from the API of [ELabFTW ](https://www.elabftw.net/).

## Setup

Please read the [README_SETUP.md](README_SETUP.md) for instructions on how to
setup this code.


## Further Reading

Please refer to the [official documentation](https://docs.indiscale.com/caosdb-crawler/) of the LinkAhead Crawler for more information.

## Contributing

Thank you very much to all contributers—[past,
present](https://gitlab.com/linkahead/linkahead/-/blob/main/HUMANS.md), and prospective
ones.

### Code of Conduct

By participating, you are expected to uphold our [Code of
Conduct](https://gitlab.com/linkahead/linkahead/-/blob/main/CODE_OF_CONDUCT.md).

### How to Contribute

* You found a bug, have a question, or want to request a feature? Please 
[create an issue](https://gitlab.com/linkahead/crawler-extensions/elabftw-cfood/-/issues).
* You want to contribute code?
    * **Forking:** Please fork the repository and create a merge request in GitLab and choose this repository as
      target. Make sure to select "Allow commits from members who can merge the target branch" under
      Contribution when creating the merge request. This allows our team to work with you on your
      request.
    * **Code style:** This project adhers to the PEP8 recommendations, you can test your code style
      using the `autopep8` tool (`autopep8 -i -r ./`).  Please write your doc strings following the
      [NumpyDoc](https://numpydoc.readthedocs.io/en/latest/format.html) conventions.
* You can also  join the LinkAhead community on
  [#linkahead:matrix.org](https://matrix.to/#/!unwwlTfOznjEnMMXxf:matrix.org).

## License

* Copyright (C) 2022 Indiscale GmbH <info@indiscale.com>

All files in this repository are licensed under a [GNU Affero General Public
License](LICENCE.md) (version 3 or later).
